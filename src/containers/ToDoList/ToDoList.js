import React, { Component } from 'react';
import Item from '../../components/Item/Item.js'
import NewItem from '../../components/NewItem/NewItem.js'
import { DestroyButton, SubmitButton } from '../../Theme/Theme.js'
import * as ToDoItemApi from '../../helpers/toDoItemApi.js'

class ToDoList extends Component {

    static defaultProps = {
        tasks: [],
        title: "List name"
    }
    
    state = {
        tasks: [],
        title: 'My List',
        draft: ''
    }

    updateList = async () => {
        this.redirectToLoginPage()
        const myTasks = await ToDoItemApi.getAllTasks() 
        this.setState({tasks: myTasks})
    }

    logout = () => {
        ToDoItemApi.logout()
        this.redirectToLoginPage()
    }

    redirectToLoginPage = () => {
        if(!ToDoItemApi.isLogged()) {
            this.props.history.push("/login");
        }
    }

    componentDidMount = () => {
        this.redirectToLoginPage()
        this.updateList()
    }

    removeAllTasks = () => {
       ToDoItemApi.removeAllTasks()
       this.updateList()
    }

    updateDraft = event => {
        this.setState({draft: event.target.value})
    }

    addTask = () => {
        const newTask = {content: this.state.draft, done: false, priority: "low"}
        ToDoItemApi.addNewTask(newTask)
        this.setState({draft: ''})
        this.updateList()
    }

    render() {
        const { title, tasks, draft } = this.state
        return (
        <div>
            <h1>{title}<div><DestroyButton onClick={this.logout}>Logout</DestroyButton></div></h1>
            {tasks.map(task => <Item task={task} updateList={this.updateList} key={'taskNumber'+task.id} />)}
            <NewItem addTask={this.addTask} updateDraft={this.updateDraft} draft={draft}/>
            <DestroyButton onClick={this.removeAllTasks}>Remove all tasks</DestroyButton>
        </div>
        )
    }
}

export default ToDoList
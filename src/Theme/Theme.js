import styled from 'styled-components'
import { Link } from 'react-router-dom'

export const SubmitButton = styled.button`
    display: block;
    border-radius: 10px;
    padding: 5px;
    color: #fff;
    margin-bottom: 10px;
    background: #232632;
`

export const TextInput = styled.input`
    padding: 5px;
    background: #232632;
    color: #d3d4d6;
    width: 100%;
    margin-right: 7px;
    border: 0px;
    -webkit-appearance: none;
`

export const Label = styled.label`
    display: flex;
    flex-direction: column;
    color: #777;
    font-size: 0.8em
    margin: 0.5em 0;
    position: relative;
`

export const Select = styled.select`
    color: #d3d4d6;
    padding: 5px;
    background: #232632;
    border: 0px;
    height: 25px;
`

export const ItemDiv = styled.div`
    background: ${props => props.done ? 'green' : '#343744'};
    border-radius: 10px;
    padding: 14px;
    margin-bottom: 7px;
`

export const RemoveButton = styled.button`
    background: #232632;
    color: red; !important;
    text-decoration: none !important;
    width: 80px;
    heigth: 32px;
    font-size: 1.7em;
    border: 2px solid white;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-left: auto;
`

export const Content = styled.div`
    float: left;
`

export const StyledLink = styled(Link)`
    background: #232632;
    color: palevioletred;
    width: 76px;
    heigth: 32px;
    justify-content: center;
    align-items: center;
    border: 2px solid white;
    font-size: 1.7em;
    text-decoration: none;
    margin-left: auto;
    display: flex;

    &:hover {
        color: white;
    }
`

export const Button = styled.button`
    background: #232632;
    color: #00a7fa;
    width: 80px;
    heigth: 32px;
    font-size: 1.7em;
    border: 0px;
    display: flex;
    justify-content: center;
    align-items: center;
`

export const DestroyButton =  styled.button`
    border-radius: 10px;
    background: red;
    padding: 5px;
    color: #fff;
    margin: 10px auto 10px;
`

export const Container = styled.div`
    display: flex;
    justify-content: space-between;
    border: 2px solid #343744;
    background: #232632;
    border-radius: 10px;
    padding: 5px;
`

export const ErrorMessage = styled.div`
    color: red;
`
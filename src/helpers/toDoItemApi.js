let listPriority = [
    {priority: 'low'}, 
    {priority: "medium"},
    {priority: "hight"}
]

let user = {
    login: "marcin",
    password: "reactjs"
}

let  logged = false

let tasks = [
    {id: 0, content: 'Do homework', done: true, priority: "low"},
    {id: 1, content: 'Visit parents', done: false, priority: "medium"},
    {id: 2, content: 'Go to the doctor', done: false, priority: "hight"}
];

let newId = 100;

export const login = (login, password) => {
    if (user.login === login &&
        user.password === password) {
            logged = true
    }
    return isLogged
}

export const isLogged = () => {
    return logged
}

export const logout = () => {
    logged = false
}

export const getAllTasks = () => { return tasks}

export const addNewTask = (newTask) => {
    newTask.id = newId
    newId = newId+1
    tasks.push(newTask)
}

export const removeTask = (taskToRemove) => {
    let indexToRemove = tasks.indexOf(taskToRemove)
    if (indexToRemove > -1) {
        tasks.splice(indexToRemove,1)
    }

}

export const removeAllTasks = () => {
    tasks = []
}

export const updateTask = (updatedTask) => {
    console.log(updatedTask.priority)
    tasks.map(task => {
        if(task.id == updatedTask.id) {
            task.content = updatedTask.content
            task.done = updatedTask.done
            task.priority = updatedTask.priority
        }
    })
}

export const findTaskById = (id) => {
    let taskToReturn = null
    tasks.map(task => {
        if(task.id == id) {
            taskToReturn = task;
        }
    })
    return taskToReturn;
}
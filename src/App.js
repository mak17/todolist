import React, { Component } from 'react';
import ToDoList from './containers/ToDoList/ToDoList.js'
import EditTask from './components/EditTask/EditTask.js'
import NotFound from './components/NotFound/NotFound.js'
import NotFoundTask from './components/NotFoundTask/NotFoundTask.js'
import Login from './components/Login/Login.js'
import './App.css'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import styled from 'styled-components'

const Container = styled.div`
    background: #2b2e39;
    margin: 0 auto;
    width: 80%;
    max-width: 600px;
    padding: 14px;
    border-radius: 14px;
    margin-top:14px;
`

class MyApp extends Component {
  render() {
    return (
      <Router>
          <Container>
            <Switch>
              <Route exact path="/" component={ToDoList} />
              <Route exect path="/task/:idTask" component={EditTask} /> 
              <Route exect path="/login" component={Login} /> 
              <Route exact path="/notFoundTask" component={NotFoundTask} />
              <Route component={NotFound} />
            </Switch>
          </Container> 
      </Router>
    )
  }
}

function App()  {
  return (
    <div>
      <MyApp />
    </div>
  );
}

export default App;

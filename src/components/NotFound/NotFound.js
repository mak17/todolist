import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom'
// component with hooks
const NotFound = ({location}) => {
    const [counter, setCounter] = useState(10)

    const countDown = () => {setCounter(counter - 1)}

    //componentDidMount or componentUpdate
    useEffect(() => {
        const id = setTimeout(countDown, 1000)

        return () => {
            clearTimeout(id)
        }
    }, [counter])
    return (
        <div>
            <p>No match for <code>{location.pathname}</code></p>
            <p>Redirect to home page in {counter} seconds</p>
            {counter === 0 && <Redirect to='/' />}
        </div>
    )
}
export  default NotFound

/*

import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'

class NotFound extends Component {

    state = {
        counter: 10
    }

    componentDidMount = () => {
        const intervalId = setInterval(this.countDown, 1000)
        this.setState({intervalId})
    }

    componentWillUnmount = () => {
        clearInterval(this.state.intervalId)
    }

    countDown = () => {
        this.setState({ counter: this.state.counter-1})
    }

    render() {
        return (
            <div>
                <p>No match for <code>{this.props.location.pathname}</code></p>
                <p>Redirect to home page in {this.state.counter} seconds</p>
                {this.state.counter === 0 && <Redirect to='/' />}
            </div>
        );
    }
}

export  default NotFound
*/
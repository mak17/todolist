import React, { Component } from 'react';
import * as ToDoItemApi from '../../helpers/toDoItemApi.js'
import { ItemDiv, RemoveButton, Content, StyledLink } from '../../Theme/Theme.js'

class Item extends Component {

    state = {
        task: this.props.task
    }

    removeTask = () => {
        ToDoItemApi.removeTask(this.state.task)
        this.props.updateList()
    }

    render() {
        const { task } = this.state
        return (
        <div>
            <ItemDiv done={task.done}>
                <Content>{task.content}({task.priority})</Content>
                <StyledLink to={`/task/${task.id}`}>Edit</StyledLink>
                <RemoveButton onClick={this.removeTask}>X</RemoveButton>
            </ItemDiv>
        </div>
        )
    }
}

export  default Item
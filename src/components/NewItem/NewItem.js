import React, { Component } from 'react';
import { Button, TextInput, Container } from '../../Theme/Theme.js'

class NewItem extends Component {

    render () {
        const { updateDraft, addTask, draft} = this.props
        return (
            <Container>
                <TextInput type="text" onChange={updateDraft} value={draft}/>
                <Button onClick={addTask}>+</Button>
            </Container>
        )
    }
}

export default NewItem
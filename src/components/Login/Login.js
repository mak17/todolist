import React, { Component } from 'react';
import { Label, TextInput, SubmitButton, ErrorMessage } from '../../Theme/Theme.js'
import { Redirect } from 'react-router-dom'
import * as ToDoItemApi from '../../helpers/toDoItemApi.js'

class Login extends Component {
    state = {
        login: "",
        password: "",
        showError: false
    }

    componentDidMount = () => {
        this.redirectToTaskPage();
        this.setState({login: "Login", password: "Password"})
    }

    login = () => {
        this.setState({showError: ToDoItemApi.login(this.state.login, this.state.password)})
        this.redirectToTaskPage();
    }

    updateLogin = event => {
        this.setState({login: event.target.value})
    }

    updatePassword = event => {
        this.setState({password: event.target.value})
    }

    redirectToTaskPage = () => {
        if(ToDoItemApi.isLogged()) {
            this.props.history.push("/");
        }
    }

    render() {
        return (
            <div>
                <h1>Login page</h1>
                {this.state.showError && <Label><ErrorMessage>Incorrect login or password!</ErrorMessage></Label>}
                <Label>
                    Login:
                    <TextInput type="text" onChange={this.updateLogin} value={this.state.login}/>
                </Label>
                <Label>
                    Password:
                    <TextInput type="password" onChange={this.updatePassword} value={this.state.password}/>
                </Label>
                <SubmitButton type="submit" onClick={this.login}>Login</SubmitButton>
            </div>
        )
    }
}

export  default Login
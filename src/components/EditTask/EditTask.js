import React, { Component } from 'react';
import * as ToDoItemApi from '../../helpers/toDoItemApi.js'
import { Formik } from 'formik'
import { SubmitButton, TextInput, Label, ErrorMessage, Select } from '../../Theme/Theme.js'

class EditTask extends Component {

    state = {
        task: {},
        disabled: false
    }

    componentDidMount = () => {
       // this.redirectToNotFoundTask()
      //  this.redirectToLoginPage()
        const myTask = ToDoItemApi.findTaskById(this.props.match.params.idTask)
        if(!myTask) {
            this.redirectToNotFoundTask()
        }
        this.setState({task: myTask, ready: true})
    }

    redirectToLoginPage = () => {
        if(!ToDoItemApi.isLogged()) {
            this.props.history.push("/login");
        }
    }

    redirectToHome = () => {
        this.props.history.push("/");
    }

    redirectToNotFoundTask = () => {
        this.props.history.push("/notFoundTask");
    }

    render() {
        return (
            <div>
                {this.state.ready && <Formik 
                    initialValues={{...this.state.task}}
                    onSubmit={(values) => { 
                        ToDoItemApi.updateTask(values)
                        this.redirectToHome()
                    }}
                    validate={(values) => {
                        let errors = {}

                        if(!values.content) {
                            errors.content = "Required"
                        } else if (values.content.length < 3) {
                            errors.content = "To short. Minimum 3 characters"
                        }

                        if(errors.length > 0) {
                            this.setState({disabled: true})
                        } else {
                            this.setState({disabled: false})
                        }
                        return errors
                    }}
                    render={({
                        values,
                        errors,
                        touched,
                        handleBlur,
                        handleChange,
                        handleSubmit,
                        isSubmitting 
                    }) => (
                        <form onSubmit={handleSubmit}>
                            <Label>
                                Content * 
                                <ErrorMessage>{errors.content}</ErrorMessage>
                                <TextInput 
                                    name="content" 
                                    onChange={handleChange}
                                    value={values.content} 
                                />
                            </Label>
                            <br/>
                            <Label>
                                Priority
                                <Select 
                                    name="priority"
                                    value={values.priority}
                                    onChange={handleChange}>
                                        <option value="low">Low</option>
                                        <option value="medium">Medium</option>
                                        <option value="hight">Hight</option>
                                </Select>
                            </Label>
                            <br/>
                            <Label>
                                Done?
                                <input type="checkbox" 
                                    name="done" 
                                    value="values.done" 
                                    checked={values.done} 
                                    onChange={handleChange}
                                /> 
                            </Label>
                            <br/>
                            <SubmitButton type="submit" disabled={this.state.disabled}>Update</SubmitButton>
                        </form>
                    )}
                />}
            </div>
        );
    }
}

export  default EditTask
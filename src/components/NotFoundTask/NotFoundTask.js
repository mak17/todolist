import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'

class NotFoundTask extends Component {

    state = {
        counter: 10
    }

    componentDidMount = () => {
        const intervalId = setInterval(this.countDown, 1000)
        this.setState({intervalId})
    }

    componentWillUnmount = () => {
        clearInterval(this.state.intervalId)
    }

    countDown = () => {
        this.setState({ counter: this.state.counter-1})
    }

    render() {
        return (
            <div>
                <p>Not found task.</p>
                <p>Redirect to home page in {this.state.counter} seconds</p>
                {this.state.counter === 0 && <Redirect to='/' />}
            </div>
        );
    }
}

export  default NotFoundTask